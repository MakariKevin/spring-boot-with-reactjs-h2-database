A single page application using React as frontend and spring boot as backend. 

This specific one is Spring Boot backend (server) – to develop REST API. Check out [Its Frontend Implementation](https://gitlab.com/MakariKevin/spring-boot-frontend-with-reactjs-h2-database)

## Tools And Technologies Used
- Java 16
- Spring Boot
- Spring MVC
- Spring Data JPA ( Hibernate)
- H2 Database
- IntelliJ IDEA

## Client-Server Architecture

![Screenshot 2022-10-25 at 09.48.38.png](./Screenshot 2022-10-25 at 09.48.38.png)

## Output

![Screenshot 2022-10-25 at 09.40.52.png](./Screenshot 2022-10-25 at 09.40.52.png)
