package com.learning.springbootwithreactjs;

import com.learning.springbootwithreactjs.model.User;
import com.learning.springbootwithreactjs.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWithReactJsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWithReactJsApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {
		this.userRepository.save(new User("James", "Pete", "jpete@gmail.com"));
		this.userRepository.save(new User("Pris", "Kimi", "pkimi@gmail.com"));
		this.userRepository.save(new User("Abdi", "Mhos", "amhos@gmail.com"));
	}
}
